<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function about()
	{

	$example = ['life' , 'edu' , 'sport' , 'and ...'];
	
	
	return view ('master.about' , compact('example'));
	}

	public function contact()
	{
		return view ('master.call-us');
	}
	public function showblog()
	{
		
    return view ('master.blog');
	}

}

