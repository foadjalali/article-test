<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article;
use App\Http\Requests\CreateArticleRequest;

class ArticlesController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except('index' , 'show');
    }




        public function index()
    {
    	$articles = Article::all();
    	return view ('master.articles' , compact('articles'));
        
    }




    public function show($id)
    {
    	$articles = Article::findOrFail($id);
    	return view ('master.showarticle' , compact('articles'));
        
    }

    public function create()
    {
        return view('master.create');
    }

    public function store(CreateArticleRequest $request)
    {


        $articles = new Article();
        
        $articles->title= $request->title;
        $articles->body= $request->body;
        $articles->save();

    	// $input = $request->all();
    	// Article::create($input);
     //    $article->save();

    	return redirect('articles');

    }
    public function destroy($id)
    {
        $dlt = Article::findOrFail($id);
        $dlt->delete();
        return redirect('articles');

    }
}
