<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	protected $fillable = [
    	'name',
    	'lable',
    	'description',
    ];
    public function role()
    {
    	return $this->belongsToMany('App\Role');
    }
    public function user()
    {
    	return $this->belongsToMany('App\user');
    }
}
