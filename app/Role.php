<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	protected $fillable = [
    	'name',
    	'lable',
    	'description',
    ];
    public function permission()
    {
    	return $this->belongsToMany('App\Permission');
    }
    public function user()
    {
    	return $this->hasMany('App\User');
    }
}
