@extends('layouts.admin')

@section('style')
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        داشبرد اموات
    </h1>
</section>
<!-- Main content -->
<section class="content">
    @include('layouts.sections.messages')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">ایجاد کاربر</h3>
                </div>
              <!-- /.box-header -->
                <form action="{{ route('users.store') }}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">نام</label>
                            <input type="text" name="name" class="form-control" value="{{ old('name') }}" id="name" placeholder="نام">
                            @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">ایمیل</label>
                            <input type="text" name="email" class="form-control" value="{{ old('email') }}" id="email" placeholder="ایمیل">
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile">موبایل</label>
                            <input type="text" name="mobile" class="form-control" value="{{ old('mobile') }}" id="mobile" placeholder="موبایل">
                            @if ($errors->has('mobile'))
                            <span class="help-block">
                                <strong>{{ $errors->first('mobile') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password">رمزعبور</label>
                            <input type="text" name="password" class="form-control" value="{{ old('password') }}" id="password" placeholder="رمزعبور">
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                        
                    <!-- ./box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">ثبت</button>
                    </div>
                </form>
            <!-- /.box-footer -->
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection

@section('js')
@endsection