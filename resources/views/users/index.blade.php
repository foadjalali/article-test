@extends('layouts.admin')

@section('style')
@endsection

@section('modal')
<!-- Modal -->
    @can('users-destroy')
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="#" id="delete_frm" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">دقت نمایید</h4>
                        </div>
                        <div class="modal-body">

                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <label for="description">آیا اطمینان دارید که می‌خواهید کاربر را پاک کنید؟</label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                            <button type="submit" class="btn btn-primary">تایید</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    داشبرد اموات
</h1>
</section>
<!-- Main content -->
<section class="content">
    @include('layouts.sections.messages')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">نمایش کاربران</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            @can('users-store')
                <a href="{{ route('users.create') }}" class="btn btn-success" style="margin-bottom: 10px" >افزودن</a>
            @endcan
            <table class="table table-bordered">
                <tr>
                    <th style="width: 10px">#</th>
                    <th>نام</th>
                    <th>موبایل</th>
                    <th>ایمیل</th>
                    <th>نقش</th>
                    <th>عملیات</th>
                </tr>
                @foreach($users as $user)
                <tr>
                    
                    <td>{{$user->name}}</td>
                    <td>{{$user->mobile}}</td>
                    <td>{{$user->email}}</td>
                    
                    <td>
                        @can('users-update')
                            <a href="{{ route('users.edit', ['id'=> $user->id]) }}">ویرایش</a>
                        @endcan

                        @can('users-destroy')
                             | <a href="#" class="delete_btn" data-toggle="modal" data-action="{{ route('users.destroy', ['id' => $user->id]) }}" data-target="#myModal" >حذف</a>
                        @endcan
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        <!-- ./box-body -->
        <div class="box-footer clearfix">
          
      </div>
      <!-- /.box-footer -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
@endsection

@section('js')
    <script>
        $('.delete_btn').on('click', function()
        {
            action = $(this).data('action');
            $('#delete_frm').attr('action',action);
        })
    </script>
@endsection