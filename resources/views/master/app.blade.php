 <!DOCTYPE html>
<html>
<head>
	<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=your_API_key"></script>
	<script>
	  tinymce.init({
	    selector: 'textarea',  // change this value according to your HTML
	    plugins : 'advlist autolink link image lists charmap print preview'
	  });
	  </script>
	  <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	    
</head>
<body>
	<div class="container">
		<div class="header" style="">
			
			@include('master.header')
		</div>
		<div style="padding-left: 20px ; text-align: right;">
			@yield('master.content')
			
		</div>
	</div>

</body>
</html> 