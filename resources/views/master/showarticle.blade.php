@extends('master.app')
@section('master.content')


  <h1>{{$articles->title}}</h1>
  
  
 
       

  	<article>
  		{{$articles-> body}}
  	</article>
  	<br>
  	<hr>
  	<button onclick="location.href='{{ url('articles') }}'">Back</button>
  	<form action="/articles/{{$articles->id}}" method="post">
  		{{csrf_field() }}
  		{{method_field('DELETE')}}
  		
  	<button type="submit" class="btn btn-primary">Delete</button>	
  	</form> 
       
 

 @stop