 <!-- Sidebar user panel -->
<div class="user-panel">
  <div class="pull-right image">
    <img src="{{ asset('images/profile.png') }}" class="img-circle" alt="User Image">
  </div>
  <div class="pull-right info">
    <p>{{Auth::user()->name}}</p>
    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
  </div>
</div>
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
  
    <li class="header">menu</li>

    <li @if(Request::routeIs('home')) class="active" @endif>
        <a href="{{ route('home') }}">
            <i class="fa fa-cogs" aria-hidden="true"></i><span>home</span>
        </a>
    </li>

   <!--@can('providers-index')
        <li @if(Request::routeIs('providers.index')) class="active" @endif>
            <a href="{{ route('providers.index') }}">
                <i class="fa fa-university" aria-hidden="true"></i><span>تامین کنندگان</span>
            </a>
        </li>  
    @endcan

    @can('products-index')
        <li @if(Request::routeIs('products.index')) class="active" @endif>
            <a href="{{ route('products.index') }}">
                <i class="fa fa-archive" aria-hidden="true"></i><span>محصولات</span>
            </a>
        </li>  
    @endcan-->

    @can('users-index')
        <li @if(Request::routeIs('users.index')) class="active" @endif>
            <a href="{{ route('users.index') }}">
                <i class="fa fa-users" aria-hidden="true"></i><span>کاربران</span>
            </a>
        </li>  
    @endcan

    @can('roles-index')
        <li @if(Request::routeIs('roles.index')) class="active" @endif>
            <a href="{{ route('roles.index') }}">
                <i class="fa fa-address-card" aria-hidden="true"></i><span>نقش ها</span>
            </a>
        </li>  
    @endcan

    @can('permissions-index')
        <li @if(Request::routeIs('permissions.index')) class="active" @endif>
            <a href="{{ route('permissions.index') }}">
                <i class="fa fa-handshake-o" aria-hidden="true"></i><span>دسترسی ها</span>
            </a>
        </li>  
    @endcan
    

</ul>