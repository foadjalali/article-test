@if(session('message'))
    <div class="row">
        <div class="col-md-12">
            <div @if(session('status') == 'error') class="alert alert-danger" @else class="alert alert-success" @endif role="alert">
                <p>{{ session('message') }}</p>
            </div>
        </div>
    </div>
@endif

@if (session('status') and session('status') != 'error')
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        </div>
    </div>
@endif