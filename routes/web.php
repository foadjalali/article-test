<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::resource('articles' , 'ArticlesController');
//Route::middleware('auth')->group(function(){
//	Route::resource('articles' , 'ArticlesController');
Route::resource('users' , 'UserController');
Route::resource('roles' , 'RoleController');
Route::resource('permissions' , 'PermissionController');


//});
Route::get('blog' , 'AboutController@showblog');
Route::get('about' , 'AboutController@about');
Route::get('contact' , 'AboutController@contact');
// Route::get('articles' , 'ArticlesController@index');
// Route::post('articles' , 'ArticlesController@store');
// Route::get('articles/create' , 'ArticlesController@create');
// Route::get('articles/{id}' , 'ArticlesController@show');
// Route::delete('articles/{id}' , 'ArticlesController@destroy');
Route::get('/home' , 'HomeController@index')->name('home');
